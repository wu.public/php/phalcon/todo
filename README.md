# TODO - phalcon
This is a very small app example using Phalcon

## Important
This application assumes that it will be hosted at the root of the server, and also that the folder name will be 'todo' (_http://127.0.0.1/todo_)
If not, you have to set the name (_and/or path_) in the file: `./public/index.php` definition `define("APP_NAME", "todo");`

## Technologies applied (_dependencies_)
| Name | Version |
| ------ | ------ |
| PHP | 7.3.3 |
| Phalcon | 4.1.2 |
| jQuery | 3.6.0 |
| Bootstrap | 5.1.3 |
| dataTables | 3.6.0 |


# Database
Edit __config.php__ file of this project, it is located in:
`./app/config/config.php`. Setup your config with your host, username and password.

```php
return new \Phalcon\Config([
    'database' => [
        'adapter'     => 'Mysql',
        'host'        => 'localhost',
        'username'    => 'user',
        'password'    => 'secret',
        'dbname'      => 'exampledb',
        'charset'     => 'utf8',
    ],
```


## Create Table

```sql
CREATE TABLE `todo` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `note` varchar(500) NOT NULL,
  `state` smallint NOT NULL DEFAULT 1,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `doneAt` datetime NULL,
  `archivedAt` datetime NULL,
  `deletedAt` datetime NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `todo` CHANGE `state` `state` smallint COMMENT '0: DELETED, 1:PENDING, 2:DONE, 3:ARCHIVED';

```