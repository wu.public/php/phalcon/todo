<?php
// defined("BASE_PATH") || define("BASE_PATH", getenv("BASE_PATH") ?: realpath(dirname(__FILE__) . "/../.."));
// defined("APP_PATH") || define("APP_PATH", BASE_PATH . "/app");
// defined("APP_NAME") || define("APP_NAME", "todo");


return new \Phalcon\Config([
    "db" => [
        "adapter"     => "Mysql",
        "host"        => "localhost",
        "username"    => "guest_user",
        "password"    => "secret()",
        "dbname"      => "phalcondb",
        "charset"     => "utf8",
    ],
    "app" => [
        "appDir"         => APP_PATH . "/",
        "coreDir"        => APP_PATH . "/core/",
        "coreEnumsDir"   => APP_PATH . "/core/enums/",
        "envDir"         => APP_PATH . "/environment/",
        "entitiesDir"    => APP_PATH . "/entities/",
        "controllersDir" => APP_PATH . "/controllers/",
        "modelsDir"      => APP_PATH . "/models/",
        "viewsDir"       => APP_PATH . "/views/",
        "pluginsDir"     => APP_PATH . "/plugins/",
        "libraryDir"     => APP_PATH . "/library/",
        "cacheDir"       => APP_PATH . "/cache/",
        "baseUri"        => "/" . APP_NAME,
    ],
    "env" => [
        "dev" => 0,
        "sta" => 1,
        "pro" => 2,
    ],
]);
