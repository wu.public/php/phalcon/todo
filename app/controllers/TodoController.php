<?php
declare(strict_types=1);

//use App\Entity\BaseResponse;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\PresenceOf;


class TodoController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $todoList = Todo::find([
            "conditions" => "state > 0", //Don't show "removed", or should I really delete it? Mmm..
            //"bind"       => [ 1 => TodoState::ARCHIVED ],
            "order"      => "state, id",
        ]);
    	$this->view->todoList=$todoList;
    }

    /**
     * Index action
     */
    public function searchAction()
    {
        $numberPage = $this->request->getQuery('page', 'int', 1);
        $parameters = Criteria::fromInput($this->di, 'Todo', $_GET)->getParams();
        $parameters['order'] = "id";

        $paginator = new Model(
            [
                'model'      => 'Todo',
                'parameters' => $parameters,
                'limit'      => 5,
                'page'       => $numberPage,
            ]
        );

        $paginate = $paginator->paginate();

        if (0 === $paginate->getTotalItems()) {
            $this->flash->notice("The search did not find any todo");

            $this->dispatcher->forward([
                "controller" => "todo",
                "action" => "index"
            ]);

            return;
        }

        $this->view->todoList = $paginate;
    }

    /**
     * Creates a new todo
     */
    public function createAction()
    {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        $validation = new Validation();
        $validation->add(
            'note',
            new PresenceOf(['message' => 'Field \'note\' is required'])
        );

        $validationMsg = $validation->validate($_POST);

        if (count($validationMsg)) {
            $resMsg = "";
            foreach ($validationMsg as $msg) {
                $resMsg .= "$msg.\n";
            }
            return json_encode(new BaseResponse(ResponseCode::ERROR, ["message"=>$resMsg]));
        }

        $todo = new Todo();
        $todo->note = $this->request->getPost("note");
        $todo->state = TodoState::PENDING;

        if (!$todo->save()) {
            $resMsg = "";
            foreach ($todo->getMessages() as $message)
                $resMsg .= "$message.\n";
            return json_encode(new BaseResponse(ResponseCode::ERROR, $resMsg));
        }

        $newTodo = Todo::findById($todo->id);

        return json_encode(new BaseResponse(
            ResponseCode::SUCCESS,
            [
                "message"=>"TODO created!",
                "todo"=>$newTodo->toJsonString()
            ]
        ));
    }

    /**
     * Saves a todo edited
     *
     */
    public function updateAction($id)
    {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        $validation = new Validation();
        $validation->add(
            'note',
            new PresenceOf(['message' => 'Field \'note\' is required'])
        );

        $data = [];
        parse_str(file_get_contents('php://input'), $data);
        $validationMsg = $validation->validate($data);

        if (count($validationMsg)) {
            $resMsg = "";
            foreach ($validationMsg as $msg)
                $resMsg .= "$msg.\n";

            return json_encode(new BaseResponse(
                ResponseCode::ERROR,
                [ "message"=>$resMsg ]
            ));
        }

        $todo = Todo::findFirstByid($id);
        if (!$todo) {
            return json_encode(new BaseResponse(
                ResponseCode::ERROR_RECORD_NOT_FOUND,
                [
                    "message"=>"TODO does not exist",
                    "id"=>$id,
                ]
            ));
        }

        $todo->note = $data["note"];
        $todo->updatedAt = date("Y-m-d H:i:s");

        if (!$todo->save()) {
            $resMsg = "";
            foreach ($todo->getMessages() as $msg)
                $resMsg .= "$msg.\n";

            return json_encode(new BaseResponse(
                ResponseCode::ERROR,
                [
                    "message"=>$resMsg,
                    "id"=>$id,
                ]
            ));
        }

        return json_encode(new BaseResponse(
            ResponseCode::SUCCESS,
            [
                "message"=>"TODO updated!",
                "id"=>$id,
                "todo"=>$todo,
            ]
        ));
    }

    /**
     * Change state to {DONE, PENDING, ARCHIVED, DELETED}
     */
    public function changeStateAction()
    {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        $validation = new Validation();
        $validation->add(
            ["id", "state"],
            new Numericality([
                "message" => [
                    "id"  => "Field 'id' is not numeric",
                    "state" => "Field 'state' is not numeric",
                ]
            ])
        );

        $validationMsg = $validation->validate($_POST);
        if (count($validationMsg)) {
            $resMsg = "";
            foreach ($validationMsg as $msg)
                $resMsg .= "$msg.\n";

            return json_encode(new BaseResponse(ResponseCode::ERROR, ["message"=>$resMsg]));
        }

        if (!TodoState::isValidValue((int)$this->request->getPost("state")))
            return json_encode(new BaseResponse(ResponseCode::ERROR, ["message"=>"State no valid"]));

        $todo = Todo::findFirstByid($this->request->getPost("id"));
        if (!$todo) {
            return json_encode(new BaseResponse(
                ResponseCode::ERROR_RECORD_NOT_FOUND,
                [
                    "message"=>"TODO does not exist",
                    "id"=>$this->request->getPost("id"),
                ]
            ));
        }
        $todo->state = $this->request->getPost("state");
        $todo->updatedAt = date("Y-m-d H:i:s");

        $message = "State updated!";
        switch ($todo->state) {
            case TodoState::DONE: {
                $todo->doneAt = date("Y-m-d H:i:s");
                $message = "Done!";
                break;
            }
            case TodoState::ARCHIVED: {
                $todo->archivedAt = date("Y-m-d H:i:s");
                $message = "TODO archived";
                break;
            }
            /* I'm really going to delete it `:D
            case TodoState::DELETED: {
                $todo->setDateDeleted(date("Y-m-d H:i:s"));
                $message = "TODO deleted!";
                break;
            }*/
        }

        //$result = $todo->state == TodoState::DELETED ? $todo->delete() : $todo->save();
        if (!$todo->save()) {
            $message = "";
            foreach ($todo->getMessages() as $msg)
                $message .= "$msg.\n";
            return json_encode(new BaseResponse(ResponseCode::ERROR, $message));
        }

        return json_encode(new BaseResponse(
            ResponseCode::SUCCESS,
            [
                "message"=>$message,
                "id"=>$todo->id,
                "state"=>$todo->state,
            ]
        ));
    }

    /**
     * Deletes a todo
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        $todo = Todo::findFirstByid($id);
        if (!$todo)
            return json_encode(new BaseResponse(ResponseCode::ERROR, ["message"=>"TODO was not found"]));

        if (!$todo->delete()) {
            $message = "";
            foreach ($todo->getMessages() as $msg)
                $message .= "$msg.\n";
            return json_encode(new BaseResponse(ResponseCode::ERROR, $message));
        }

        $this->flash->success("todo was deleted successfully");
        return json_encode(new BaseResponse(
            ResponseCode::SUCCESS,
            [
                "message"=>"TODO deleted!",
                "id"=>$id
            ]
        ));
    }

}
