<?php
namespace core\enums;

/**
 * @author	Walter Urbina	<walter.urbina.at@gmail.com>
 * @since	2020-03-01
 */
abstract class BaseEnum {
	private static $constCacheArray = NULL;

	private static function getConstants()
	{
		if (self::$constCacheArray == NULL)
			self::$constCacheArray = [];

		$calledClass = get_called_class();
		if (!array_key_exists($calledClass, self::$constCacheArray)) {
			$reflect = new \ReflectionClass($calledClass);
			self::$constCacheArray[$calledClass] = $reflect->getConstants();
		}
		return self::$constCacheArray[$calledClass];
	}

	public static function isValidName($name, $strict=FALSE)
	{
		$constants = self::getConstants();
		#log_message("debug", "BASIC_ENUM - isValidName - constants: ". json_encode($constants));
		if ($strict)
			return array_key_exists($name, $constants);

		$keys = array_map('strtolower', array_keys($constants));
		return in_array(strtolower($name), $keys);
	}

	public static function isValidValue($value, $strict=TRUE)
	{
		$values = array_values(self::getConstants());
		#log_message("debug", "BASIC_ENUM - isValidValue - values: $value in ". json_encode($values));
		return in_array($value, $values, $strict);
	}

	public static function getName($value)
	{
		$constants = self::getConstants();
		return array_search($value, $constants);
	}

	public static function getKey($value)
	{
		$constants = self::getConstants();
		return array_search ($value, $constants);
	}

	public static function getAsArray()
	{
		return self::getConstants();
	}
}
