<?php

//namespace App\Entity;

class BaseResponse
{
	public $code;
	public $data;

	public function __construct() {
		$args = func_get_args();
		$i = func_num_args();

		if (method_exists($this, $f='__construct'.$i))
			call_user_func_array(array($this, $f), $args);
	}

	private function __construct1($_data) {
		$this->code = 0;//CODE_SUCCESS;
		$this->data = $_data;
	}

	private function __construct2($_code ,$_data) {
		$this->code = $_code;
		$this->data = $_data;
	}

	public function toString() {
		return json_encode(array("code"=>$this->code, "data"=>$this->data));
	}

	public static function fromObject($_obj) {
		return new BaseResponse((int)$_obj->code, $_obj->data);
	}
}