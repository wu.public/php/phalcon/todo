<?php

include_once APP_PATH . '/core/enums/BaseEnum.php';

abstract class ResponseCode extends \core\enums\BaseEnum
{
	const SUCCESS					= 0;

	const INFO 						= 10;
	const INFO_EMPTY_RESULT 		= 11;

	const WARNING_RECORD_NOT_SAVED	= 31;

	const ERROR						= 50; //General error
	const ERROR_RECORD_NOT_FOUND	= 51;
}
