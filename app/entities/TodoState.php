<?php

include_once APP_PATH . '/core/enums/BaseEnum.php';

abstract class TodoState extends \core\enums\BaseEnum
{
	const DELETED	= 0; //In this case this refers to an action
	const PENDING   = 1;
	const DONE      = 2;
	const ARCHIVED	= 3;
}