<?php
declare(strict_types=1);

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 * PSR-4
 */
$loader->registerDirs(
    [
        $config->app->coreEnumsDir,
        $config->app->controllersDir,
        $config->app->entitiesDir,
        $config->app->modelsDir
    ]
)->register();
