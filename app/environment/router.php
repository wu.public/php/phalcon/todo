<?php
declare(strict_types=1);

use Phalcon\Mvc\Router\Group as RouterGroup;

$router = $di->getRouter();
$config = $di->getConfig();
$baseuri = $config->app->baseUri;

$router->removeExtraSlashes(true);

//TODO: ?
$router->handle($_SERVER["REQUEST_URI"]);

// Home
$router->addGet("$baseuri/about", "About::index");
$router->addGet("$baseuri/author","About::author");


// TODOs
$router->addGet("$baseuri", "Todo::index");
$router->addGet("$baseuri/search", "Todo::index");
$router->addPost("$baseuri", "Todo::create");
$router->addPost("$baseuri/change-state", "Todo::changeState");
$router->addPatch("$baseuri/{id:[0-9]+}", "Todo::update");
$router->addDelete("$baseuri/{id:[0-9]+}", "Todo::delete");
