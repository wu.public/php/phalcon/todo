<?php

class Todo extends \Phalcon\Mvc\Model
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $note;

    /**
     *
     * @var integer
     */
    public $state;

    /**
     *
     * @var string with date format, format Y-m-d H:i:s
     */
    public $createdAt;

    /**
     *
     * @var string with date format, format Y-m-d H:i:s
     */
    public $updatedAt;

    /**
     *
     * @var string with date format, format Y-m-d H:i:s
     */
    public $doneAt;

    /**
     *
     * @var string with date format, format Y-m-d H:i:s
     */
    public $archivedAt;

    /**
     *
     * @var string with date format, format Y-m-d H:i:s
     */
    /*protected $deletedAt;

    public function setDeleteDate($date_str) {
        $this->deletedAt = $date_str;
    }*/

    public function getStateName() {
        $name = "unknown";
        switch ($this->state) {
            case TodoState::PENDING: {
                $name = "pending";
                break;
            }
            case TodoState::DONE: {
                $name = "done";
                break;
            }
            case TodoState::ARCHIVED: {
                $name = "done";
                break;
            }
        }
        return $name;
    }


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("phalcondb");
        $this->setSource("todo");
    }

    public function toJsonString(){
        return json_encode(
            [
                "id"        => $this->id,
                "note"      => $this->note,
                "state"     => $this->state,
                "stateName" => $this->getStateName(),
                "createdAt" => $this->createdAt,
                "updatedAt" => $this->updatedAt,
                "doneAt"    => $this->doneAt,
                "archivedAt"=> $this->archivedAt,
            ]
        );
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Todo[]|Todo|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null): \Phalcon\Mvc\Model\ResultsetInterface
    {
        //$parameters['order'] = "id,state";
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Todo|\Phalcon\Mvc\Model\ResultInterface|\Phalcon\Mvc\ModelInterface|null
     */
    public static function findFirst($parameters = null): ?\Phalcon\Mvc\ModelInterface
    {
        return parent::findFirst($parameters);
    }

    public static function findById($id): ?\Phalcon\Mvc\ModelInterface
    {
        return parent::findFirst($id);
    }

}
