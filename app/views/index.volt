<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TODO-s</title>

    <link rel="shortcut icon" href="img/favicon.ico">

    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" /> -->
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" /> -->
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.4/datatables.min.css"/> -->

    {{ stylesheet_link('/third_party/font-awesome/v4.7.0/css/all.min.css') }}
    {{ stylesheet_link('/third_party/bootstrap/css/bootstrap.min.css') }} <!--v5.1.3-->
    <!-- {{ stylesheet_link('/third_party/bootstrap/fonts/icons/bootstrap-icons.css') }} -->
    {{ stylesheet_link('/third_party/datatables/css/datatables.min.css') }} <!--v1.11.4-->
    {{ stylesheet_link('/css/site.css') }}

</head>
<body class="bg-secondary">
<div class="container">
    <div class="col-xl-6 col-lg-7 col-md-9 col-sm-11 col-xs-12 m-5 p-2 rounded mx-auto bg-light shadow">
        <!-- App title section -->
        <div class="row m-1 p-4">
            <div class="col">
                <div class="p-1 h1 text-primary text-center fw-bold mx-auto display-inline-block">
                    <i class="fa fa-check bg-primary text-white rounded p-2"></i>
                    TODO-s
                </div>
            </div>
        </div>

        <?= $this->getContent() ?>

    </div>
</div>




<div class="toast-container position-absolute p-3 top-0 end-0">

    <div id="toastDefault" class="toast align-items-center border-0" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="d-flex">
            <div class="toast-body">
                <div id="toastDefaultMessage"></div>
            </div>
            <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
    </div>

    <div id="toastPrimary" class="toast align-items-center text-white bg-primary border-0" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="d-flex">
            <div class="toast-body">
                <div id="toastPrimaryMessage">Primary</div>
            </div>
            <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
    </div>

    <div id="toastInfo" class="toast align-items-center text-white bg-info border-0" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="d-flex">
            <div class="toast-body">
                <div id="toastInfoMessage">Info</div>
            </div>
            <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
    </div>

    <div id="toastSuccess" class="toast align-items-center text-white bg-success border-0" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="d-flex">
            <div class="toast-body">
                <div id="toastSuccessMessage">Success!</div>
            </div>
            <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
    </div>

    <div id="toastWarning" class="toast align-items-center text-white bg-warning border-0" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="d-flex">
            <div class="toast-body">
                <div id="toastWarningMessage">Warning!</div>
            </div>
            <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
    </div>

    <div id="toastError" class="toast align-items-center text-white bg-danger border-0" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="d-flex">
            <div class="toast-body">
                <div id="toastErrorMessage">An error ocurred!</div>
            </div>
            <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
    </div>
</div> <!-- End Toast -->


<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> -->
<!-- <script src="https://cdn.datatables.net/v/bs5/dt-1.11.4/datatables.min.js"></script> -->

{{ javascript_include('/third_party/jquery/js/jquery.min.js') }}
{{ javascript_include('/third_party/bootstrap/js/bootstrap.bundle.min.js') }}
{{ javascript_include('/third_party/datatables/js/datatables.min.js') }}


<script type="text/javascript">
    const BASE_URI = "{{ url() }}";
    console.log("base_uri",BASE_URI)
</script>
{{ javascript_include('/js/ajaxService.js') }}
{{ javascript_include('/js/toast.js') }}


<!--{{ javascript_include('/js/todo_state.js') }}-->
{{ javascript_include('/js/app.js') }}


</body>
</html>
