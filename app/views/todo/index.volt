
<!-- Create todo section -->
<div class="row m-1 p-3">
    <div class="col col-11 mx-auto">
        <div class="row bg-white rounded shadow-sm p-2 add-todo-wrapper align-items-center justify-content-center">
            <div class="col">
                <input  id="txtSearch"
                        class="form-control form-control-lg border-0 add-todo-input bg-transparent rounded"
                        type="text"
                        placeholder="Search or create new"
                        autocomplete="off" >
            </div>
            <div id="addDivContainer" class="col-auto px-0 mx-0 mr-2 d-none">
                <button id="addBtn" type="button" class="btn btn-primary">Add</button>
            </div>
        </div>
    </div>
</div>

<!-- <div class="row mx-5 my-2 border-black-25 border-bottom"></div> -->

<!-- View options section -->
<div id="filterOptions" class="row mx-1 px-5 justify-content-end {% if todoList|length == 0 %} d-none {% endif %} ">
    <div class="col-auto d-flex align-items-center">
        <!-- <label class="mx-4 view-opt-label"><b>Filter:</b></label> -->
        <label class="text-secondary mx-2 view-opt-label">State</label>
        <!-- <select id="slcState" class="custom-select custom-select-sm btn my-2"> -->
        <select id="slcState" class="form-select form-select-sm" autocomplete="off">
            <option value="-1" selected>All</option>
            <option value="1">Pending</option>
            <option value="2">Done</option>
            <option value="3">Archived</option>
        </select>
    </div>
    <!-- <div class="col-auto d-flex align-items-center px-1">
        <label class="text-secondary view-opt-label mx-2">Sort</label>
        <select class="form-select form-select-sm">
            <option value="added-date-asc" selected>Added date</option>
            <option value="due-date-desc">Due date</option>
        </select>
        <i class=""></i>
        <i class="fa fa-sort-numeric-asc text-info btn mx-0 px-0 pl-1" data-toggle="tooltip" data-placement="bottom" title="Ascending"></i>
        <i class="fa fa-sort-numeric-desc text-info btn mx-0 px-0 pl-1 d-none" data-toggle="tooltip" data-placement="bottom" title="Descending"></i>
    </div> -->
</div>

<!-- Todo list section -->
<div class="row mx-1 px-5 pb-3 w-80">
    <div class="col mx-auto text-center">

        <div id="emptyList" class="py-4 my-4 {% if todoList|length > 0 %} d-none {% endif %}">
            <h1 >Empty list</h1>
        </div>

        <table id="todoTable" class="table table-hover table-sm {% if todoList|length == 0 %} d-none {% endif %}">
            <thead>
                <tr>
                    <th class="no-sort"></th>
                    <th class="no-sort"></th>
                    <th class="no-sort"></th>
                </tr>
            </thead>
            <tbody>
                {% for t in todoList %}
                    {% if t.state == 3 %}
                    {% set css_row_class = 'table-danger' %}
                    {% elseif t.state == 2 %}
                    {% set css_row_class = 'table-success' %}
                    {% else %}
                    {% set css_row_class = '' %}
                    {% endif %}

                <tr id="todoRow_{{t.id}}"
                    class="todo-row hide-action-archive hide-action-restore {{ css_row_class }}"
                    onmouseover="onMouseOverRow({{t.id}})"
                    onmouseleave="onMouseLeaveRow({{t.id}})"
                    >
                    <td class="text-center col-sm-1">
                        <a class="state-chk align-middle" onclick="changeState({{t.id}})">
                            {% if t.state == 1 %}
                                <i class="fa fa-square-o btn px-1 py-1" /></i>
                            {% elseif t.state == 2 %}
                                <i class="fa fa-check bg-success text-white rounded px-1 py-1"></i>
                            {% endif %}
                        </a>
                    </td>
                    <td class="text-start col-sm-8" onclick="editTodo({{t.id}})">
                        <textarea class="todo-note todo-input form-control border-0 rounded d-none"
                                type="text"
                                onkeyup="onTodoEditing({{t.id}}, this, event)"
                                autocomplete="off"
                                rows="1"
                                ></textarea>
                        <span class="todo-note todo-label">
                            {{t.note}}
                        </span>
                    </td>
                    <td class="text-center col-sm-3">
                        <a class="h4 cursor-pointer action-save d-none" title="save" data-bs-toggle="tooltip" data-bs-placement="top" onclick="update({{t.id}})"><i class="fa fa-save text-primary"></i></a>
                        <a class="h4 cursor-pointer action action-archive mx-2" title="archive" data-bs-toggle="tooltip" data-bs-placement="top" onclick="archive({{t.id}})"><i class="fa fa-trash text-danger"></i></a>
                        <a class="h4 cursor-pointer action action-restore mx-1" title="restore" data-bs-toggle="tooltip" data-bs-placement="top" onclick="restore({{t.id}})"><i class="fa fa-undo text-primary"></i></a>
                        <a class="h4 cursor-pointer action action-restore mx-1" title="delete"  data-bs-toggle="tooltip" data-bs-placement="top" onclick="remove({{t.id}})"><i class="fa fa-close text-danger"></i></a>
                    </td>
                </tr>
                {% endfor %}
            </tbody>
        </table>

    </div>
</div>


<script type="text/javascript">
    var todoList = {{ todoList | json_encode }};
    console.log("todoList",todoList);

</script>
