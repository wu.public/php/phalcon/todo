<?php
declare(strict_types=1);

use \Phalcon\Di\FactoryDefault;

define("BASE_PATH", dirname(__DIR__));
define("APP_PATH", BASE_PATH . "/app");
define("APP_NAME", "todo");

try {
    /**
     * The FactoryDefault Dependency Injector automatically register
     * the right services providing a full stack framework
     */
    $di = new FactoryDefault();

    /**
     * Shared configuration service
     */
    $di->setShared('config', function () {
        return include APP_PATH . "/config/config.php";
    });

    define("APP_ENV", $di->getConfig()->env->pro);

    ini_set("display_errors"        , (APP_ENV != $di->getConfig()->env->pro ? "1" : "no"));
    ini_set("display_startup_errors", (APP_ENV != $di->getConfig()->env->pro ? "1" : "no"));
    error_reporting(APP_ENV != $di->getConfig()->env->pro ? 1 : 0);
    /**
     * Load and configure services
     */
    include APP_PATH . "/environment/services.php";

    /**
     * Handle routes
     */
    include APP_PATH . "/environment/router.php";

    /**
     * Call the autoloader service.  We don't need to keep the results.
     */
    $di->getLoader();

    /**
     * Handle the request
     */
    $application = new \Phalcon\Mvc\Application($di);

    $response = $application->handle(
        $_SERVER["REQUEST_URI"]
    );

    $response->send();

}
catch (\Exception $e) {
    echo $e->getMessage() . '<br>';
    echo '<pre>' . $e->getTraceAsString() . '</pre>';
}
