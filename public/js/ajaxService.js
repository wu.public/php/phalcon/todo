const ajaxService = function() {
    function validateConfig(config) {
        if (!config)
            return false;
        if (!config.hasOwnProperty("url"))
            return false;
        //if (!config.hasOwnProperty("data"))
        //    return false;
        //if (!config.hasOwnProperty("type"))
        //    return false;
        return true;
    }

    function doGetReq(url, onSuccess=null, onError=null) {
        var successCallback = onSuccess ? onSuccess : function(data) {
            console.log("onSuccess","data:",data);
        }

        var errorCallback = onError ? onError : function(e) {
            console.error("onError",e);
        }

        $.ajax({
            url: BASE_URI + url,
            method: "GET",
            //contentType: "application/json",
            dataType: "json",
            success: successCallback,
            error: errorCallback
        });
    }

    function doPostReq(url, data=null, onSuccess=null, onError=null) {
        var successCallback = onSuccess ? onSuccess : function(data) {
            console.log("onSuccess","data:",data);
        }

        var errorCallback = onError ? onError : function(e) {
            console.error("onError",e);
        }

        $.ajax({
            url: BASE_URI + url,
            method: "POST",
            data: data ? data : [],
            //contentType: "application/json",
            dataType: "json",
            success: successCallback,
            error: errorCallback
        });
    }

    function doPatchReq(url, data=null, onSuccess=null, onError=null) {
        var successCallback = onSuccess ? onSuccess : function(data) {
            console.log("onSuccess","data:",data);
        }

        var errorCallback = onError ? onError : function(e) {
            console.error("onError",e);
        }

        $.ajax({
            url: BASE_URI + url,
            method: "PATCH",
            data: data ? data : [],
            //contentType: "application/json",
            dataType: "json",
            success: successCallback,
            error: errorCallback
        });
    }

    function doDeleteReq(url, onSuccess=null, onError=null) {
        var successCallback = onSuccess ? onSuccess : function(data) {
            console.log("onSuccess","data:",data);
        }

        var errorCallback = onError ? onError : function(e) {
            console.error("onError",e);
        }

        $.ajax({
            url: BASE_URI + url,
            method: "DELETE",
            dataType: "json",
            success: successCallback,
            error: errorCallback
        });
    }

    return {
        get: function(config) {
            var isConfigValid = validateConfig(config);
            if (!isConfigValid) {
                console.error("ajaxService", "invalid request", "usage: ajaxService.get({url:string, onSuccess: fun *optional, onError: fun *optional})");
                return null;
            }
            doGetReq(config.url, config.data, config.onSuccess, config.onError);
        },
        post: function(config) {
            var isConfigValid = validateConfig(config);
            if (!isConfigValid) {
                console.error("ajaxService", "invalid request", "usage: ajaxService.post({url:string, data:json, onSuccess: fun *optional, onError: fun *optional})");
                return null;
            }
            doPostReq(config.url, config.data, config.onSuccess, config.onError);
        },
        patch: function(config) {
            var isConfigValid = validateConfig(config);
            if (!isConfigValid) {
                console.error("ajaxService", "invalid request", "usage: ajaxService.post({url:string, data:json, onSuccess: fun *optional, onError: fun *optional})");
                return null;
            }
            doPatchReq(config.url, config.data, config.onSuccess, config.onError);
        },
        delete: function(config) {
            var isConfigValid = validateConfig(config);
            if (!isConfigValid) {
                console.error("ajaxService", "invalid request", "usage: ajaxService.post({url:string, data:json, onSuccess: fun *optional, onError: fun *optional})");
                return null;
            }
            doDeleteReq(config.url, config.onSuccess, config.onError);
        }
    };
}();