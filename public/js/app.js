
var addBntVisible = false
var todoSelected = null

var jqSearchTxt = null
var jqTable = null

var dataTable = null

var editionInProcess = false

const DATA_TABLE_CONFIG = {
    searching: false,
    //columnDefs: [ { orderable: false, targets: [0,1,2] } ],
    order: [],
    columnDefs: [
        { targets: 'no-sort', orderable: false  }
    ],
    dom: '<"top"r>t<"row"<"col-sm-6"i><"col-sm-6"p>><"clear">',
    //dom: '<"row"<"col-sm-4"r>>t<"row"<"col-sm-4"i><float-right<"col-sm-4"p>>><"clear">',
}

$(document).ready( function() {

    jqTable = $("#todoTable")

    jqSearchTxt = $("#txtSearch")
    jqSearchTxt.on("focus", unfocusPrevious)
    jqSearchTxt.on("keyup", onInputSearch)

    $("#addBtn").on("click", create)

    if (typeof todoList !== 'undefined' && todoList.length > 0) {
        dataTable = jqTable.DataTable(DATA_TABLE_CONFIG)
    }

    $("#slcState").on("change", function(){
        filterState(this.value)
    })

})

function onInputSearch(event) {
    let val = event.target.value

    // hide add button & show all rows
    if (val == "") {
        $("#addDivContainer").addClass("d-none")
        $(".todo-row").removeClass("d-none")
        return
    }
    else
        $("#addDivContainer").removeClass("d-none")

    if (event.keyCode == 13) {
        create()
        return
    }

    // hide all rows
    $(".todo-row").addClass("d-none")

    let foundItems = todoList.filter( todo => todo.note.match(val) )
    if (foundItems.length == 0)
        return

    // show only matches row
    foundItems.some((element) => {
        $("#todoRow_"+element.id).removeClass("d-none")
    })

}

function clearSearch() {
    // show all rows
    $(".todo-row").removeClass("d-none")
    // clear search
    jqSearchTxt.val("")
    $("#addDivContainer").addClass("d-none")
}


function filterState(state) {
    //if (state == TodoState.ARCHIVED.value)

    let foundItems = todoList.filter( todo =>
        state == TodoState.ALL.value || todo.state == state
    )
    console.log("filterState",foundItems)

    dataTable.clear().draw()
    if (foundItems.length == 0) {
        //$("#emptyList").removeClass("d-none")
        //dataTable.clear().draw()
        //dataTable.destroy()
        return
    }

    foundItems.forEach(item => {
        insertRow(item)
    })
}


function create() {
    if ( jqSearchTxt.val() == "") {
        Toast.warning("You must specify note!")
        return
    }

    ajaxService.post({
        url: "/",
        data: {note: jqSearchTxt.val()},
        onSuccess: onCreated,
        onError: onErrorRequest
    })
}

function onCreated(res) {
    console.log("onCreated", res)
    if (res.code == 0) {
        let newTodo = JSON.parse(res.data.todo)

        Toast.success(res.data.message)
        todoList.push(newTodo)
        insertRow(newTodo)
        clearSearch()
    }
    else {
        Toast.error(res.data.message)
    }
}

function insertRow(todo) {
    let row_cssClass = ""
    let disableInput = "disabled"
    let check_html = ""

    switch (parseInt(todo.state)) {
        case TodoState.PENDING.value:
            disableInput = ""
            check_html =    "<a class='state-chk align-middle' onclick='changeState("+ todo.id +")'>"+
                                "<i class='fa fa-square-o text-disabled btn px-1 py-1'></i>"+
                            "</a>";
            break
        case TodoState.DONE.value:
            row_cssClass = "table-success"
            check_html =    "<a class='state-chk align-middle' onclick='changeState("+ todo.id +")'>"+
                                "<i class='fa fa-check bg-success text-white rounded px-1 py-1'></i>"+
                            "</a>";
            break
        case TodoState.ARCHIVED.value:
            row_cssClass = "table-danger"
            break;
    }

    let row = "<tr id='todoRow_"+ todo.id +"' "+
                " class='todo-row hide-action-archive hide-action-restore "+ row_cssClass +" '"+
                " onmouseover='onMouseOverRow("+ todo.id +")' "+
                " onmouseleave='onMouseLeaveRow("+ todo.id +")' >"+
                    "<td class='text-center col-sm-1'>"+
                        check_html +
                    "</td>"+
                    "<td class='text-start col-sm-8' onclick='editTodo("+ todo.id +")' >"+
                        "<textarea class='todo-note todo-input form-control border-0 rounded d-none'"+
                                " type='text' "+
                                " onkeyup='onTodoEditing("+ todo.id +", this, event)' "+
                                " autocomplete='off' "+
                                " rows='1' "+
                        "></textarea> "+
                        "<span class='todo-note todo-label'> " + todo.note +"</span> "+
                    "</td>"+
                    "<td class='text-center col-sm-3'>"+
                        "<a class='h4 cursor-pointer action-save d-none' title='save' onclick='update("+ todo.id +")'><i class='fa fa-save text-primary'></i></a>"+
                        "<a class='h4 cursor-pointer action action-archive mx-2' title='archive' onclick='archive("+ todo.id +")'><i class='fa fa-trash text-danger'></i></a>"+
                        "<a class='h4 cursor-pointer action action-restore mx-1' title='restore' onclick='restore("+ todo.id +")'><i class='fa fa-undo text-primary'></i></a>"+
                        "<a class='h4 cursor-pointer action action-restore mx-1' title='delete' onclick='remove("+ todo.id +")'><i class='fa fa-close text-danger'></i></a>"+
                    "</td>"+
                "</tr>";
    //jqTable.find("tbody").prepend(row)

    // For first row
    createDataTable()

    dataTable.row.add($(row)).draw()
}

function createDataTable() {
    if (dataTable != null || todoList.length == 0)
        return

    $("#filterOptions").removeClass("d-none")
    $("#emptyList").addClass("d-none")
    jqTable.removeClass("d-none")

    if (dataTable == null)
        dataTable = jqTable.DataTable(DATA_TABLE_CONFIG)
}


function onMouseOverRow(id) {
    let row = $("#todoRow_" + id)
    let todo = todoList.find( todo => todo.id == id )
    if (todo.state == TodoState.ARCHIVED.value)
        row.removeClass("hide-action-restore")
    else
        row.removeClass("hide-action-archive")
}

function onMouseLeaveRow(id) {
    let row = $("#todoRow_" + id)
    row.addClass("hide-action-archive hide-action-restore")
}

function editTodo(id) {
    // Restore original value if the TODO was not update
    if (todoSelected)
        unfocusPrevious()

    todoSelected = todoList.find( todo => todo.id == id )
    if (todoSelected.state != TodoState.PENDING.value)
        return

    editionInProcess = true
    let row = $("#todoRow_" + todoSelected.id)
    row.find(".todo-label").addClass("d-none")
    row.find(".todo-input").val(todoSelected.note)
                           .removeClass("d-none")
                           .focus()
}

/** @deprecated use editTodo() */
function onFocusItem(id, element) {
    // Restore original value if the TODO was not update
    if (todoSelected)
        unfocusPrevious()

    todoSelected = todoList.find( todo => todo.id == id )
    if (todoSelected.state == TodoState.DONE.value)
        return

    editionInProcess = true
    $(element).removeClass("bg-transparent")
}

function unfocusPrevious() {
    if (!editionInProcess)
        return

    let row = $("#todoRow_" + todoSelected.id)
    //row.find(".todo-note").addClass("bg-transparent")
    //row.find(".todo-input").val(todoSelected.note)

    row.find(".todo-input").addClass("d-none")
    row.find(".todo-label").removeClass("d-none")
    row.find(".action-save").addClass("d-none")
    row.find(".action-archive").removeClass("d-none")
    editionInProcess = false
}

function onTodoEditing(id, element, event=null) {
    if (event == null && event.keyCode != 13)
        return

    // Without changes
    if (element.value == todoSelected.note) {
        let row = $("#todoRow_" + id)
        row.find(".action-save").addClass("d-none")
        row.find(".action-archive").removeClass("d-none")
        editionInProcess = false
        return
    }

    if (event.keyCode == 13) {
        update(id)
        return
    }

    editionInProcess = true
    let row = $("#todoRow_" + id)
    row.find(".action-save").removeClass("d-none")
    row.find(".action-archive").addClass("d-none")
}

function update(id) {
    let value = $("#todoRow_" + id).find(".todo-input").val()
    if (!value || value == "") {
        Toast.warning("The note can't be empty")
        return
    }

    ajaxService.patch({
        url: "/"+id,
        data: {
            note: value
        },
        onSuccess: onUpdated,
        onError: onErrorRequest
    })
}

function onUpdated(res) {
    console.log("onUpdated", res)
    if (res.data.id) {
        unfocusPrevious()
        //editionInProcess = false
        //let row = $("#todoRow_" + res.data.id)
        //row.find(".action-save").addClass("d-none")
        //row.find(".action-archive").removeClass("d-none")
    }

    let row = $("#todoRow_" + res.data.id)
    if (res.code == 0) {
        Toast.success(res.data.message)

        let todo = res.data.todo
        let index = todoList.findIndex((todo => todo.id == todo.id))
        todoList[index].note = todo.note
        row.find(".todo-label").html(todo.note)
    }
    else {
        Toast.error(res.data.message)
        // restore original value
        if (res.data.id != null) {
            let todo = todoList.find( todo => todo.id == res.data.id )
            row.find(".todo-label").val(todo.note)
        }
    }
}



function archive(id) {
    changeState(id, TodoState.ARCHIVED)
}
function restore(id) {
    changeState(id, TodoState.PENDING)
}

function changeState(id, state=undefined) {
    let todo = todoList.find( todo => todo.id == id )
    if (todo == undefined) {
        Toast.info("TODO not found")
        return
    }
    if (state == undefined)
        state = new TodoState(todo.state).value == TodoState.PENDING.value
            ? TodoState.DONE
            : TodoState.PENDING

    ajaxService.post({
        url: "/change-state",
        data: {
            id: id,
            state: state.value
        },
        onSuccess: onStateChanged,
        onError: onErrorRequest
    })
}

function remove(id) {
    ajaxService.delete({
        url: "/"+id,
        onSuccess: onStateChanged,
        onError: onErrorRequest
    })
}

function onStateChanged(res) {
    console.log("onStateChanged", res)
    if (todoSelected)
        unfocusPrevious()

    if (res.code == 0) {
        Toast.success(res.data.message)

        let row = $("#todoRow_"+res.data.id)
        let todo = todoList.find( todo => todo.id == res.data.id )
        let index = todoList.findIndex((todo => todo.id == res.data.id))
        todoList[index].state = res.data.state

        row.removeClass("table-success table-danger")
        //row.find(".todo-note").prop("disabled",true)

        if (todo.state == TodoState.DONE.value) {
            row.addClass("table-success")
            row.find(".state-chk").html('<i class="fa fa-check bg-success text-white rounded px-1 py-1"></i>')
        }
        else if (todo.state == TodoState.PENDING.value) {
            //row.find(".todo-note").prop("disabled",false)
            row.find(".state-chk").removeClass('d-none')
                                  .html('<i class="fa fa-square-o btn px-1 py-1"></i>')

        }
        else if (todo.state == TodoState.ARCHIVED.value) {
            row.addClass("table-danger")
            row.find(".state-chk").addClass('d-none')
        }
        else {
            todoList.splice(index,1)
            dataTable.row(row).remove().draw()
            row.remove()
        }

        onMouseLeaveRow(todo.id)
    }
    else {
        Toast.error(res.data.message)
    }
}



function onErrorRequest(error) {
    console.error(error)
    Toast.error("An error ocurred!")
}


class TodoState {
    static ALL = new TodoState(-1)
    static DELETED = new TodoState(0)
    static PENDING = new TodoState(1)
    static DONE = new TodoState(2)
    static ARCHIVED = new TodoState(3)

    constructor(value) {
      this.value = value
    }
}
