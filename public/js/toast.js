const Toast = function() {
    class ToastType {
        static Default = new ToastType(0)
        static Info = new ToastType(1)
        static Success = new ToastType(2)
        static Warning = new ToastType(3)
        static Error = new ToastType(4)
        static Primary = new ToastType(5)

        constructor(value) {
            this.value = value
        }
    }
    var mType = ToastType.Default;

    /**
     *
     * @param ToastType type
     * @param string message
     * @returns
     */
    function show(type, message) {
        let id = undefined
        switch (type.value) {
            case ToastType.Default.value:
                id = 'toastDefault'
                break
            case ToastType.Info.value:
                id = 'toastInfo'
                break
            case ToastType.Success.value:
                id = 'toastSuccess'
                break
            case ToastType.Warning.value:
                id = 'toastWarning'
                break
            case ToastType.Error.value:
                id = 'toastError'
                break
            case ToastType.Primary.value:
                id = 'toastPrimary'
                break
            default:
                console.warn("Toast not configured!")
                return
        }
        let toastHtml = document.getElementById(id)
        if (message != null) {
            let toastMsg = document.getElementById(id + 'Message')
            toastMsg.innerText = message
        }

        let toast = new bootstrap.Toast(toastHtml)
        toast.show()
    }

    return {
        default : function(msg) { show(ToastType.Default, msg) },
        primary : function(msg) { show(ToastType.Primary, msg) },
        info    : function(msg) { show(ToastType.Info, msg) },
        success : function(msg) { show(ToastType.Success, msg) },
        warning : function(msg) { show(ToastType.Warning, msg) },
        error   : function(msg) { show(ToastType.Error, msg) },
    };
}();
