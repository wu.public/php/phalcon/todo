class TodoState {
    static DELETED = new TodoState(0)
    static PENDING = new TodoState(1)
    static DONE = new TodoState(2)
    static ARCHIVED = new TodoState(3)

    constructor(value) {
      this.value = value
    }
}